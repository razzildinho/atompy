from django.conf.urls import url

from atompy import views

urlpatterns = [
    url(r'^$', views.ExampleView.as_view(), name='example'),
]
